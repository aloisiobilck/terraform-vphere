variable "vsphere_datacenter" {
  default = "DC-XXX"
}

variable "vsphere_network_vlan" {
  default = "VLAN"
}

variable "vsphere_datastore" {
  default = "storage_xxx"
}

variable "vsphere_virtual_machine_template" {
  default = "vm_template"
}

variable "vsphere_virtual_machine_name" {
  default = "hostname_vm"
}

variable "vsphere_virtual_machine_cpu" {
  default = "1"
}

variable "vsphere_virtual_machine_memory" {
  default = "1024"
}

variable "vsphere_virtual_machine_ip_address" {
  default = "ip_vlan"
}

variable "vsphere_virtual_machine_ip_netmask" {
  default = mask_vlan
}

variable "vsphere_virtual_machine_ip_gateway" {
  default = "gw_vlan"
}

variable "vsphere_virtual_machine_ip_domain" {
  default = "dominio"
} 
 
variable "virtual_machine_dns_servers" {
  default = ["DNS1", "DNS2"]
}

variable "virtual_machine_domain" {
  default = ["dominio"]
}


variable "vsphere_folder" {
  default = "Folder_vm"
  
}
